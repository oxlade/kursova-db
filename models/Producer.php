<?php

namespace models;

use core\Core;

class Producer
{
    protected static $tableName = 'producer';

    public static function add($name, $photoPath)
    {
        do {
            $fileName = uniqid() . '.png';
            $newPath = "files/producer/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($photoPath, $newPath);
        Core::getInstance()->db->insert(self::$tableName, [
            'name' => $name,
            'photo' => $fileName
        ]);
    }

    public static function deletePhotoFile($id)
    {
        $row = Producer::getProducerById($id);
        $photoPath = 'files/producer/' . $row['photo'];
        if (is_file($photoPath))
            unlink($photoPath);
    }

    public static function changePhoto($id, $newPhoto)
    {
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() . '.png';
            $newPath = "files/producer/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        Core::getInstance()->db->update(self::$tableName, [
            'photo' => $fileName
        ], [
            'id' => $id
        ]);
    }

    public static function getProducerById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($row))
            return $row[0];
        return null;
    }

    public static function delete($id)
    {
        self::deletePhotoFile($id);
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function update($id, $newName, $newInfo)
    {
        Core::getInstance()->db->update(self::$tableName, [
            'name' => $newName,
            'info' => $newInfo
        ], [
            'id' => $id
        ]);
    }

    public static function getProducers()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        return $rows;
    }

    public static function isNameExists($name)
    {
        $producer = Core::getInstance()->db->select(self::$tableName, '*', [
            'name' => $name
        ]);
        return !empty($producer);
    }
}