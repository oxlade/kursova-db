<?php

namespace models;

use core\Core;
use core\Utils;

class Wallet
{
    protected static $tableName = 'hardware_wallet';

    public static function normalizationFilesArray($files)
    {
        $NormalnyArreyfiles = array();
        foreach($files['files'] as $k => $l) {
            foreach($l as $i => $v) {
                $NormalnyArreyfiles[$i][$k] = $v;
            }
        }
        return $NormalnyArreyfiles;
    }

    public static function add($row, $files)
    {
        $files = self::normalizationFilesArray($files);

        $filesArray = [];
        $mainPhoto = null;
        foreach($files as $file)
        {
            do {
                $fileName = uniqid() . ".jpg";
                $newPath = "files/wallet/{$fileName}";
            } while(file_exists($newPath));
            move_uploaded_file($file['tmp_name'],$newPath);
            if($file['name']===$row['main_photo'])
            {
                $mainPhoto = $fileName;
            }
            else
            {
                $filesArray[] = $fileName;
            }
        }
        $arrayStrJSON = json_encode($filesArray);
        $row['photos'] = $arrayStrJSON;
        $row['main_photo'] = $mainPhoto;

        $row['size'] = self::setSize($row['height'], $row['width'], $row['length']);
        $row['connection'] = json_encode($row['connection']);
        $row['compatibility'] = json_encode($row['compatibility']);
        $fieldsList = ['name', 'producer_id', 'price', 'quantity_on_stock', 'main_feature', 'description',
            'supported_coins', 'connection', 'compatibility', 'warranty', 'size', 'weight', 'visible','main_photo','photos'];
        $row = Utils::filterArray($row, $fieldsList);
        foreach ($row as $key => $value)
            if (empty($value))
                $row[$key] = null;
        Core::getInstance()->db->insert(self::$tableName, $row);
    }

    public static function setSize($height, $width, $length)
    {
        $size = [$height, $width, $length];
        if (strlen(implode($size))==0)
            return null;
        var_dump($size);
        for ($i = 0; $i < count($size); $i++)
            if (empty($size[$i]))
                $size[$i] = '-';
        return implode(' x ', $size);
    }

    public static function delete($id)
    {
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function update($id, $row)
    {
        $fieldsList = ['name', 'producer_id', 'price', 'quantity_on_stock',
            'short_description', 'description', 'visible'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->update(self::$tableName, $row, [
            'id' => $id
        ]);
    }

    public static function getWalletById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($row)) {
            $row[0]['connection'] = json_decode($row[0]['connection']);
            $row[0]['compatibility'] = json_decode($row[0]['compatibility']);
            $row[0]['photos'] = json_decode($row[0]['photos']);
            return $row[0];
        }
        return null;
    }

    public static function getWallets()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        for ($i = 0; $i < count($rows); $i++) {
            $rows[$i]['connection'] = json_decode($rows[$i]['connection']);
            $rows[$i]['compatibility'] = json_decode($rows[$i]['compatibility']);
            $rows[$i]['photos'] = json_decode($rows[$i]['photos']);
        }
        return $rows;
    }

    public static function getWalletsByProducerId($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'producer_id' => $id
        ]);
        for ($i = 0; $i < count($rows); $i++) {
            $rows[$i]['connection'] = json_decode($rows[$i]['connection']);
            $rows[$i]['compatibility'] = json_decode($rows[$i]['compatibility']);
            $rows[$i]['photos'] = json_decode($rows[$i]['photos']);
        }
        return $rows;
    }

    public static function isNameExists($name)
    {
        $wallet = Core::getInstance()->db->select(self::$tableName, '*', [
            'name' => $name
        ]);
        return !empty($wallet);
    }
}