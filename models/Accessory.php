<?php

namespace models;

use core\Core;
use core\Utils;

class Accessory
{
    protected static $tableName = 'accessory';

    public static function add($row, $files)
    {
        $files = Utils::normalizationFilesArray($files);

        $filesArray = [];
        $mainPhoto = null;
        foreach($files as $file)
        {
            do {
                $fileName = uniqid() . ".jpg";
                $newPath = "files/accessory/{$fileName}";
            } while(file_exists($newPath));
            move_uploaded_file($file['tmp_name'],$newPath);
            if($file['name']===$row['main_photo'])
            {
                $mainPhoto = $fileName;
            }
            else
            {
                $filesArray[] = $fileName;
            }
        }
        $arrayStrJSON = json_encode($filesArray);
        $row['photos'] = $arrayStrJSON;
        $row['main_photo'] = $mainPhoto;

        $fieldsList = ['name', 'producer_id', 'price', 'quantity_on_stock',
            'description', 'universal', 'visible','main_photo','photos'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->insert(self::$tableName, $row);
    }

    public static function delete($id)
    {
        Core::getInstance()->db->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function update($id, $row)
    {
        $fieldsList = ['name', 'producer_id', 'price', 'quantity_on_stock',
            'description', 'universal', 'visible'];
        $row = Utils::filterArray($row, $fieldsList);
        Core::getInstance()->db->update(self::$tableName, $row,[
            'id' => $id
        ]);
    }

    public static function getAccessoryById($id)
    {
        $row = Core::getInstance()->db->select(self::$tableName, '*', [
            'id' => $id
        ]);
        if (!empty($row)) {
            $row[0]['photos'] = json_decode($row[0]['photos']);
            return $row[0];
        }
        return null;
    }

    public static function getAccessories()
    {
        $rows = Core::getInstance()->db->select(self::$tableName);
        for ($i = 0; $i < count($rows); $i++)
            $rows[$i]['photos'] = json_decode($rows[$i]['photos']);
        return $rows;
    }

    public static function getAccessoriesByProducerId($id)
    {
        $rows = Core::getInstance()->db->select(self::$tableName, '*', [
            'producer_id' => $id
        ]);
        for ($i = 0; $i < count($rows); $i++)
            $rows[$i]['photos'] = json_decode($rows[$i]['photos']);
        return $rows;
    }

    public static function isNameExists($name)
    {
        $accessory = Core::getInstance()->db->select(self::$tableName, '*', [
            'name' => $name
        ]);
        return !empty($accessory);
    }
}