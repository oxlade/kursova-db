<?php

namespace models;

use core\Core;
use core\Utils;

class User
{
    protected static $tableName = 'user';

    public static function add($login, $password, $first_name, $last_name)
    {
        \core\Core::getInstance()->db->insert(self::$tableName, [
            'login' => $login,
            'password' => self::hashPassword($password),
            'first_name' => $first_name,
            'last_name' => $last_name
        ]);
    }

    public static function hashPassword($password){
        return md5($password);
    }
    public static function update($id, $updatesArray)
    {
        $updatesArray = Utils::filterArray($updatesArray, ['first_name', 'last_name']);
        \core\Core::getInstance()->db->update(self::$tableName, $updatesArray, [
            'id' => $id
        ]);
    }

    public static function isLoginExists($login)
    {
        $user = Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login
        ]);
        return !empty($user);
    }

    public static function verify($login, $password){
        $user = Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => $password
        ]);
        return !empty($user);
    }

    public static function getUserByLoginAndPassword($login, $password){
        $user = Core::getInstance()->db->select(self::$tableName, '*', [
            'login' => $login,
            'password' => self::hashPassword($password)
        ]);
        if (!empty($user))
            return $user[0];
        return null;
    }

    public static function getCustomers()
    {
        $rows = Core::getInstance()->db->select(self::$tableName,'*',[
            'access_level' => 1
        ]);
        return $rows;
    }

    public static function authenticate($user){
        $_SESSION['user'] = $user;
    }

    public static function logoutUser(){
        unset($_SESSION['user']);
    }

    public static function isUserAuthenticated(){
        return isset($_SESSION['user']);
    }

    public static function getCurrentUser(){
        return $_SESSION['user'];
    }

    public static function isAdmin(){
        $user = User::getCurrentUser();
        return $user['access_level'] == 10;
    }
}