<?php

namespace models;

class Cart
{
    public static function addWallet()
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart']['wallets'] = [];
            $_SESSION['cart']['accessories'] = [];
        }
        if (!empty($_POST['product_id']))
            $_SESSION['cart']['wallets'][$_POST['product_id']] += $_POST['quantity'];
    }

    public static function addAccessory()
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart']['wallets'] = [];
            $_SESSION['cart']['accessories'] = [];
        }
        if (!empty($_POST['product_id']))
            $_SESSION['cart']['accessories'][$_POST['product_id']] += $_POST['quantity'];
    }

    public static function update()
    {
        if (!empty($_POST['product_id'])) {
            $product_type = self::getProductType($_POST['product_type']);
            $_SESSION['cart'][$product_type][$_POST['product_id']] = $_POST['quantity'];
        }
    }

    public static function delete()
    {
        if (!empty($_POST['product_id'])) {
            $product_type = self::getProductType($_POST['product_type']);
            unset($_SESSION['cart'][$product_type][$_POST['product_id']]);
        }
    }

    public static function getCartProducts()
    {
        if (is_array($_SESSION['cart'])) { //
            if(empty($_SESSION['cart']['accessories']) && empty($_SESSION['cart']['wallets']))
                return null;
            else {
                $result = [];
                $products = [];
                foreach ($_SESSION['cart']['wallets'] as $key => $quantity) {
                    $wallet = Wallet::getWalletById($key);
                    $products['wallets'][] = ['product' => $wallet, 'quantity' => $quantity];
                }
                foreach ($_SESSION['cart']['accessories'] as $key => $quantity) {
                    $accessory = Accessory::getAccessoryById($key);
                    $products['accessories'][] = ['product' => $accessory, 'quantity' => $quantity];
                }
                $result['wallets'] = $products['wallets'];
                $result['accessories'] = $products['accessories'];
                $result['total_price'] = self::getTotalPrice();
                return $result;
            }
        }
        return null;
    }

    public static function getTotalPrice()
    {
        $totalPrice = 0;
        foreach ($_SESSION['cart']['wallets'] as $key => $quantity) {
            $wallet = Wallet::getWalletById($key);
            $totalPrice += $wallet['price'] * $quantity;
        }
        foreach ($_SESSION['cart']['accessories'] as $key => $quantity) {
            $accessory = Accessory::getAccessoryById($key);
            $totalPrice += $accessory['price'] * $quantity;
        }
        return $totalPrice;
    }

    public static function getProductType($productType)
    {
        if ($productType == 'Wallet')
            return 'wallets';
        return 'accessories';
    }

}