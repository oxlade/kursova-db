$(document).ready(function () {

    $('.incrementBtn').click(function (e) {
        e.preventDefault();

        let quantity = $(this).closest('.input-group').find('.inputQty').val();
        let value = parseInt(quantity, 10);
        value = isNaN(value) ? 0 : value;
        if (value < 10) {
            value++;
            $(this).closest('.input-group').find('.inputQty').val(value);
        }
    });
    $('.decrementBtn').click(function (e) {
        e.preventDefault();

        let quantity = $(this).closest('.input-group').find('.inputQty').val();
        let value = parseInt(quantity, 10);
        value = isNaN(value) ? 0 : value;
        if (value > 1) {
            value--;
            $(this).closest('.input-group').find('.inputQty').val(value);
        }
    });

    $('.addToCart').click(function (e) {
        e.preventDefault();

        let product_type = this.id;
        let product_id = $(this).val();

        $.ajax({
            method: "POST",
            url: `/cart/add${product_type}`,
            data: {
                'product_id': product_id,
                'quantity': 1
            },
            success: function (response) {
                alertify.success('Added to cart!');
            }
        });
    })

    $(document).on('click', '.updateQty', function () {
        let quantity = $(this).closest('.input-group').find('.inputQty').val();
        let product_id = $(this).val();
        let product_type = this.id;

        $.ajax({
            method: "POST",
            url: `/cart/update`,
            data: {
                'product_id': product_id,
                'quantity': quantity,
                'product_type': product_type
            },
            success: function (response) {
                $("#totalPrice").load(location.href + " #totalPrice");
            }
        });
    });

    $(document).on('click', '.deleteItem', function (){
        let product_id = $(this).val();
        let product_type = this.id;

        $.ajax({
            method: "POST",
            url: `/cart/delete`,
            data: {
                'product_id': product_id,
                'product_type': product_type
            },
            success: function (response) {
                $("#cartTable").load(location.href + " #cartTable");
                $("#totalPrice").load(location.href + " #totalPrice");
            }
        });
    });
});