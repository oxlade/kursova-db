import detectEthereumProvider from '@metamask/detect-provider';
const button_connect = document.querySelector('.button-connect');
button_connect.addEventListener('click', (event) => {
    connectWallet();
});
const button_disconnect = document.querySelector('.button-disconnect');
button_disconnect.addEventListener('click', (event) => {
    // window.ethereum.on('disconnect', handler: (error: ProviderRpcError) => void);
});
let walletAddress = "";

const connectWallet = async () => {
    if (typeof window != "undefined" && typeof window.ethereum != "undefined") {
        try {
            const accounts = await window.ethereum.request({
                method: "eth_requestAccounts"
            });
            walletAddress = accounts[0];
            console.log(accounts[0]);

            button_connect.innerHTML = walletAddress && walletAddress.length > 0
                ? `Connected ${walletAddress.substring(0, 5)}...${walletAddress.substring(38, 42)}`
                : "Connect wallet";
        } catch (error) {
            console.log(error.message);
        }
    } else {
        console.log("Install MetaMask!");
    }
}

const getCurrentWalletConnected = async () => {
    if (typeof window != "undefined" && typeof window.ethereum != "undefined") {
        try {
            const accounts = await window.ethereum.request({
                method: "eth_accounts"
            });
            if (accounts.length > 0) {
                walletAddress = accounts[0];
                console.log(accounts[0]);
            }
            button_connect.innerHTML = walletAddress.length > 0
                ? `Connected ${walletAddress.substring(0, 4)}...${walletAddress.substring(38, 42)}`
                : "Connect wallet";
        } catch (error) {
            console.log(error.message);
        }
    } else {
        console.log("Install MetaMask!");
    }
}

const addWalletListener = async () =>{
    if (typeof window != "undefined" && typeof window.ethereum != "undefined") {
        // const accounts = await window.ethereum.request({
        //     method: "eth_accounts"
        // });
        window.ethereum.on("accountsChanged", (accounts) => {
            walletAddress = accounts[0];
            console.log(accounts[0]);
        });
    } else {
        walletAddress = "";
        console.log("Install MetaMask!");
    }
}
window.onload = function (){
    getCurrentWalletConnected();
    addWalletListener();
    console.log(walletAddress);
}