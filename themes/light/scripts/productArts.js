let ProductFiles = document.querySelector(".InputProductJs");
let imageProductsDiv = document.querySelector(".imageBasket");
let btnForResetAddProd = document.querySelector(".btnForResetAddProd");

btnForResetAddProd.addEventListener("click",(event)=>
{
    ProductFiles.value = "";
    imageProductsDiv.innerHTML = "";

});



ProductFiles.addEventListener("change",(event)=>
{
    let index = 0;
   if(ProductFiles.files.length !== 0)
   {
        for(let file of ProductFiles.files)
       {
           let reader = new FileReader();
           reader.readAsDataURL(file);

           reader.onload = function ()
           {
               let img = document.createElement("img");
               let div = document.createElement("div");
               let divCard = document.createElement("div");
               let input = document.createElement("input");
               let label = document.createElement("label");
               /*
               let h = document.createElement("p");
               h.classList.add("h5");
               h.classList.add("mb-3");
               h.classList.add("text-center");
               h.innerHTML = "Оберіть головну фотографію";*/
               label.setAttribute("for",file['name'])
               label.innerHTML = "Обрати";
               input.setAttribute("type","radio")
               input.setAttribute("value","Check")
               input.setAttribute("autocomplete","off")
               input.setAttribute("name","main_photo")
               input.setAttribute("value", file['name'])
               input.setAttribute("id", file['name'])
               input.setAttribute("selected", "selected")
               if(ProductFiles.files.length === index)
               {
                   input.checked = true;
                   label.innerHTML = "Обрано";

               }


               imageProductsDiv.classList.add("mb-4");
               img.classList.add("p-1");
               img.style.height = "70px";
               div.classList.add("col");
               div.classList.add("mx-auto");
               divCard.classList.add("card");
               div.appendChild(divCard);
               divCard.appendChild(img);
               divCard.appendChild(input);
               divCard.appendChild(label);
               img.src = reader.result;
               imageProductsDiv.appendChild(div);
               label.classList.add("btn");
               label.classList.add("btnOberitJs");
               label.classList.add("w-50");
               label.classList.add("mx-auto");
               label.classList.add("m-1");
               label.classList.add("btn-outline-success")
               input.classList.add("btn-check");

               let labelsProducts = document.querySelectorAll(".btnOberitJs");
               for (let elem of labelsProducts)
               {
                   elem.addEventListener("click",(obj)=>
                   {
                       for (let elem of labelsProducts)
                       {
                           elem.innerHTML="Обрати";
                       }
                       elem.innerHTML="Обрано";
                   })
               }
           }
           index++;
       }
   }
});



