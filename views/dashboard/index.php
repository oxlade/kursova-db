<?php
\core\Core::getInstance()->pageParams['title'] = 'Dashboard';
/** @var array $customers */
/** @var array $products */

?>
<div class="container-fluid overflow-auto h-100">
    <section id="minimal-statistics">
        <div class="row">
            <div class="col-12 mt-3 mb-1">
                <h3 class="text-uppercase text-center">Main stats</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-3 my-2">
                <div class="card text-white bg-red-linear rounded-4">
                    <div class="card-content ">
                        <div class="card-body ">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3><?=count($customers)?></h3>
                                    <span>Total customers</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 my-2 ">
                <div class="card text-white bg-green-linear rounded-4">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3>278</h3>
                                    <span>Number of purchases</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 my-2 ">
                <div class="card text-white bg-blue-linear rounded-4">
                    <div class="card-content ">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3>278</h3>
                                    <span>Average purchase</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 my-2 ">
                <div class="card text-white bg-orange-linear rounded-4">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3>278</h3>
                                    <span>Total earned</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3 my-2 "></div>
            <div class="col-3 my-2 ">
                <div class="card text-white bg-aqua-linear rounded-4">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3><?=count($products)?></h3>
                                    <span>Number of posted products</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 my-2 ">
                <div class="card text-white bg-darkred-linear rounded-4">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="align-self-center me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
                                         class="bi bi-people-fill" viewBox="0 0 16 16">
                                        <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7Zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216ZM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                                    </svg>
                                </div>
                                <div class="media-body text-right">
                                    <h3><?php $sum = 0;foreach ($products as $key => $value) $sum += $value['price']; echo number_format((float)$sum/count($products), 2, '.', '');?></h3>
                                    <span>Avg price of posted products</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 my-2 "></div>
        </div>
</div>



<!---->
<!--        <div class="row">-->
<!--            <div class="col-xl-6 col-md-12">-->
<!--                <div class="card overflow-hidden">-->
<!--                    <div class="card-content">-->
<!--                        <div class="card-body cleartfix">-->
<!--                            <div class="media align-items-stretch">-->
<!--                                <div class="align-self-center">-->
<!--                                    <i class="icon-pencil primary font-large-2 mr-2"></i>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4>Total Posts</h4>-->
<!--                                    <span>Monthly blog posts</span>-->
<!--                                </div>-->
<!--                                <div class="align-self-center">-->
<!--                                    <h1>18,000</h1>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-xl-6 col-md-12">-->
<!--                <div class="card">-->
<!--                    <div class="card-content">-->
<!--                        <div class="card-body cleartfix">-->
<!--                            <div class="media align-items-stretch">-->
<!--                                <div class="align-self-center">-->
<!--                                    <i class="icon-speech warning font-large-2 mr-2"></i>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4>Total Comments</h4>-->
<!--                                    <span>Monthly blog comments</span>-->
<!--                                </div>-->
<!--                                <div class="align-self-center">-->
<!--                                    <h1>84,695</h1>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-xl-6 col-md-12">-->
<!--                <div class="card">-->
<!--                    <div class="card-content">-->
<!--                        <div class="card-body cleartfix">-->
<!--                            <div class="media align-items-stretch">-->
<!--                                <div class="align-self-center">-->
<!--                                    <h1 class="mr-2">$76,456.00</h1>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4>Total Sales</h4>-->
<!--                                    <span>Monthly Sales Amount</span>-->
<!--                                </div>-->
<!--                                <div class="align-self-center">-->
<!--                                    <i class="icon-heart danger font-large-2"></i>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-xl-6 col-md-12">-->
<!--                <div class="card">-->
<!--                    <div class="card-content">-->
<!--                        <div class="card-body cleartfix">-->
<!--                            <div class="media align-items-stretch">-->
<!--                                <div class="align-self-center">-->
<!--                                    <h1 class="mr-2">$36,000.00</h1>-->
<!--                                </div>-->
<!--                                <div class="media-body">-->
<!--                                    <h4>Total Cost</h4>-->
<!--                                    <span>Monthly Cost</span>-->
<!--                                </div>-->
<!--                                <div class="align-self-center">-->
<!--                                    <i class="icon-wallet success font-large-2"></i>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--</div>-->