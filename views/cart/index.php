<?php
/** @var array|null $products */
//unset($_SESSION['cart']['wallets']['1']);

//var_dump($products);
?>
<div id="cartTable">
    <?php if (!empty($products)) : ?>
        <h1 class="text-center">Cart</h1>
        <div class="row  text-center">
            <div class="col-3">
                Photo
            </div>
            <div class="col-3">
                Name
            </div>
            <div class="col-2">
                Price
            </div>
            <div class="col-2">
                Quantity
            </div>
            <div class="col-2 ">
                Delete product from cart
            </div>
        </div>
        <?php if (is_array($products['wallets'])) : foreach ($products['wallets'] as $key => $value) : ?>
            <div class="row bg-white rounded-5 my-2 h-15 align-items-center justify-content-center text-center ">
                <div class="col-3 ">
                    <strong>Photo</strong>
                </div>
                <div class="col-3">
                    <strong><?= $value['product']['name'] ?></strong>
                </div>
                <div class="col-2">
                    <strong>$ <?= $value['product']['price'] ?></strong>
                </div>
                <div class="col-2">
                    <div class="input-group w-75 m-auto">
                        <button class="input-group-text decrementBtn updateQty" id="Wallet"
                                value="<?= $value['product']['id'] ?>">-
                        </button>
                        <input type="text" class="form-control text-center bg-white inputQty text-black"
                               value="<?= $value['quantity'] ?>" disabled>
                        <button class="input-group-text incrementBtn updateQty" id="Wallet"
                                value="<?= $value['product']['id'] ?>">+
                        </button>
                    </div>
                </div>
                <div class="col-2">
                    <button href="#" class="btn btn-danger rounded-3 w-50 deleteItem" id="Wallet"
                            value="<?= $value['product']['id'] ?>">Delete
                    </button>
                </div>
            </div>
        <?php endforeach; endif; ?>
        <?php if (is_array($products['accessories'])) : foreach ($products['accessories'] as $key => $value) : ?>
            <div class="row bg-white rounded-5 my-2 h-15 align-items-center justify-content-center text-center">
                <div class="col-3 ">
                    <strong>Photo</strong>
                </div>
                <div class="col-3">
                    <strong><?= $value['product']['name'] ?></strong>
                </div>
                <div class="col-2">
                    <strong>$ <?= $value['product']['price'] ?></strong>
                </div>
                <div class="col-2">
                    <div class="input-group w-75 m-auto">
                        <button class="input-group-text decrementBtn updateQty" id="Accessory"
                                value="<?= $value['product']['id'] ?>">-
                        </button>
                        <input type="text" class="form-control text-center bg-white inputQty text-black"
                               value="<?= $value['quantity'] ?>" disabled>
                        <button class="input-group-text incrementBtn updateQty" id="Accessory"
                                value="<?= $value['product']['id'] ?>">+
                        </button>
                    </div>
                </div>
                <div class="col-2">
                    <button href="#" class="btn btn-danger rounded-3 w-50 deleteItem" id="Accessory"
                            value="<?= $value['product']['id'] ?>">Delete
                    </button>
                </div>
            </div>
        <?php endforeach; endif; ?>

        <div class="row  text-center my-4 h-10">
            <div class="col-3"></div>
            <div class="col-3"></div>
            <div class="col-2 border-top pb-2 border-2">
                <div class="mt-3"><strong class="fs-3 mt-3">Total</strong></div>
            </div>
            <div class="col-2 border-top pb-2 border-2">
                <div class="mt-3" id="totalPrice"><strong class="fs-3 mt-3">$ <?= $products['total_price'] ?></strong></div>
            </div>
            <div class="col-2 border-top pb-2 border-2">
                <a href="#" class="btn btn-success rounded-3 w-100 rounded-4 mt-3">Continue</a>
            </div>
        </div>
    <?php else: ?>
        <div class="row bg-white rounded-5 my-3 h-10 align-items-center justify-content-center text-center shadow-sm border border-primary">
            <div class="col-3"></div>
            <div class="col-6">
                <h1 class="fs-1 fw-bolder">Your cart is empty</h1>
            </div>
            <div class="col-3">
                <a href="/product" class="btn btn-success fs-4 rounded-4">Continue shopping</a>
            </div>
        </div>
        <div class="row  my-3 h-25 align-items-center justify-content-center text-center">
          <div class="col">
              <img src="/images/static/emptyCart.png" alt="" class="h-100">
          </div>
        </div>
    <?php endif; ?>
</div>
<?php //if (!empty($products)) : ?>

<?php //endif;?>
