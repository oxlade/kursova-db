<?php
/** @var array $wallet1 */
/** @var array $wallet2 */
/** @var array $wallets */
/** @var array $comparisonElems */
/** @var string $error */
\core\Core::getInstance()->pageParams['title'] = 'Comparison';
?>
<h1 class="my-2 text-center">Comparison</h1>
<h5 class="my-3 text-muted text-center">WHICH HARDWARE WALLET IS RIGHT FOR YOU?</h5>
<!--<h2 class="my-2 text-center text-primary">--><? //=$wallet1['name']?><!-- vs --><? //=$wallet2['name']?><!--</h2>-->
<?php if (!empty($error)): ?>
    <div id="nameHelp" class="form-text error text-center"><?= $error ?> </div>
<?php endif; ?>
<form action="" method="post">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">
                <button class="btn btn-outline-primary w-100">Compare</button>
            </th>
            <th scope="col">
                <div class="input-group">
                    <label for="wallet1"><span class="input-group-text"
                                               id="inputGroup-sizing-default">Wallet 1</span></label>
                    <select name="wallet1" id="wallet1" class="form-control form-select text-center me-5">
                        <?php foreach ($wallets as $wallet): ?>
                            <option value="<?= $wallet['id'] ?>" <?php if ($wallet['name'] == $wallet1['name']) echo 'selected'; ?>><?= $wallet['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </th>
            <th scope="col">
                <div class="input-group">
                    <label for="wallet2"><span class="input-group-text"
                                               id="inputGroup-sizing-default">Wallet 2</span></label>
                    <select name="wallet2" id="wallet2" class="form-control form-select text-center me-5">
                        <?php foreach ($wallets as $wallet): ?>
                            <option value="<?= $wallet['id'] ?>" <?php if ($wallet['name'] == $wallet2['name']) echo 'selected'; ?>><?= $wallet['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0; $i < count($comparisonElems); $i++): ?>
            <tr>
                <th scope="row" class="text-center align-middle">
                    <!--                                --><?php //if (str_contains($comparisonElems[$i], '_'))
                    //                                    echo ucfirst(str_replace('_', ' ', $comparisonElems[$i]));
                    //                                else
                    //                                    echo $comparisonElems[$i];
                    //                                ?>
                    <?= $comparisonElems[$i] ?>
                </th>
                <td class="text-center">
                    <?php if (!is_array($wallet1[$comparisonElems[$i]])): ?>
                        <?php if (!empty($wallet1[$comparisonElems[$i]])): ?>
                            <span class=""><?= $wallet1[$comparisonElems[$i]] ?></span>
                        <?php else : ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-dash-lg" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M2 8a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11A.5.5 0 0 1 2 8Z"/>
                            </svg>
                        <?php endif; ?>
                    <?php else : ?>
                        <ul class="list-group list-group-flush">
                            <?php foreach ($wallet1[$comparisonElems[$i]] as $item): ?>
                                <li class="list-group-item bg-transparent w-25 mx-auto text-center"><?= $item ?></li>
                            <?php endforeach; ?>
                        </ul>

                    <?php endif; ?>
                </td>
                <td class="text-center">
                    <?php if (!is_array($wallet2[$comparisonElems[$i]])): ?>
                        <?php if (!empty($wallet2[$comparisonElems[$i]])): ?>
                            <span class=""><?= $wallet2[$comparisonElems[$i]] ?></span>
                        <?php else : ?>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-dash-lg" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                      d="M2 8a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11A.5.5 0 0 1 2 8Z"/>
                            </svg>
                        <?php endif; ?>
                    <?php else : ?>
                        <ul class="list-group list-group-flush ">
                            <?php foreach ($wallet2[$comparisonElems[$i]] as $item): ?>
                                <li class="list-group-item bg-transparent w-25 mx-auto text-center"><?= $item ?></li>
                            <?php endforeach; ?>
                        </ul>

                    <?php endif; ?>
                </td>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
</form>