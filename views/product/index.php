<?php
\core\Core::getInstance()->pageParams['title'] = 'Products';

use \models\User;

/** @var array $products */
/** @var array $producers */

?>
<div class="main">
    <div class="container-fluid">
        <div class="row h-75">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-white text-black sidebar collapse mh-100 pt-2">
                <span class="fs-4 ms-5">Dashboard</span>
                <hr>
                <div class="position-sticky pt-3 sidebar-sticky fs-5">
                    <form action="" method="post">
                        <div class="mb-3">
                            <label for="product_type" class="form-label">Product type</label>
                            <select name="product_type" id="product_type" class="form-control form-select">
                                <option value="all">All</option>
                                <option value="wallet">Wallet</option>
                                <option value="accessory">Accessory</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="producer_id" class="form-label">Product producer</label>
                            <select name="producer_id" id="producer_id" class="form-control form-select">
                                <option value="all">All</option>
                                <?php foreach ($producers as $producer): ?>
                                    <option value="<?= $producer['id'] ?>"><?= $producer['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="row-cols-1">
                            <button class="btn btn-success">Filter</button>
                        </div>
                    </form>
                </div>

            </nav>

            <main class="col-md-9 ">
                <div class="container-fluid overflow-auto h-100">
                    <section id="minimal-statistics">
                        <div class="row h-100 my-1">
                            <?php foreach ($products as $product):
                                if (array_key_exists('supported_coins', $product)) $item = 'wallet';
                                else $item = 'accessory';
                                ?>
                                <div class="col-4 my-1 h-50">
                                    <div class="card bg-white rounded-5 border-0 h-100 align-items-center">
                                        <a href="/product/view<?= ucfirst($item) ?>/<?= $product['id'] ?>" >
                                        <div class="text-center h-50">
                                        <img src="/images/static/<?= $item ?>.png" class=" h-100 w-50" alt="...">
                                        </div>
                                        </a>
                                        <div class="card-body text-black w-100 text-center">
                                            <?php if ($item == 'wallet' && !empty($product['main_feature'])): ?>
                                                <div class="m-auto border-primary border rounded-2 fs-6 w-75"><?= $product['main_feature'] ?></div>
                                            <?php else: ?>
                                                <br>
                                            <?php endif; ?>
                                            <h4 class="card-title text-center my-2"><?= $product['name'] ?></h4>

                                            <?php if ($product['quantity_on_stock'] != 0): ?>
                                                <span class="text-black ">In stock<br></span>
                                            <?php else: ?>
                                                <p class="text-black">Not in stock</p>
                                            <?php endif; ?>
                                            <?php if (!User::isAdmin()): ?>
                                                <div class="mt-2">
                                                    <a href="/product/view<?= ucfirst($item) ?>/<?= $product['id'] ?>"
                                                       class="btn btn-secondary w-30 mb-2 mh-100 py-2 rounded-4">More</a>
                                                        <button href="#"
                                                                class="btn btn-success bg-green-linear w-60 mb-2 py-2 rounded-4 addToCart"
                                                                data-bs-toggle="modal" value="<?=$product['id']?>" id="<?= ucfirst($item) ?>"
                                                                data-bs-target="#modalDelete<?= $product['id'] ?>">
                                                            Buy for $<?= $product['price'] ?></button>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        </div>
                    </section>
                </div>
            </main>
        </div>
    </div>
</div>


<!--<ul class="nav flex-column ">-->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link active" aria-current="page" href="/dashboard/">-->
<!--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
<!--                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
<!--                 stroke-linejoin="round" class="feather feather-home align-text-bottom mb-1"-->
<!--                 aria-hidden="true">-->
<!--                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>-->
<!--                <polyline points="9 22 9 12 15 12 15 22"></polyline>-->
<!--            </svg>-->
<!--            Dashboard-->
<!--        </a>-->
<!--    </li>-->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="#">-->
<!--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
<!--                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
<!--                 stroke-linejoin="round" class="feather feather-file align-text-bottom "-->
<!--                 aria-hidden="true">-->
<!--                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>-->
<!--                <polyline points="13 2 13 9 20 9"></polyline>-->
<!--            </svg>-->
<!--            Orders-->
<!--        </a>-->
<!--    </li>-->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="#">-->
<!--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
<!--                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
<!--                 stroke-linejoin="round" class="feather feather-shopping-cart align-text-bottom "-->
<!--                 aria-hidden="true">-->
<!--                <circle cx="9" cy="21" r="1"></circle>-->
<!--                <circle cx="20" cy="21" r="1"></circle>-->
<!--                <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>-->
<!--            </svg>-->
<!--            Products-->
<!--        </a>-->
<!--    </li>-->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="/dashboard/customers">-->
<!--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
<!--                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
<!--                 stroke-linejoin="round" class="feather feather-users align-text-bottom "-->
<!--                 aria-hidden="true">-->
<!--                <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>-->
<!--                <circle cx="9" cy="7" r="4"></circle>-->
<!--                <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>-->
<!--                <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>-->
<!--            </svg>-->
<!--            Customers-->
<!--        </a>-->
<!--    </li>-->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="#">-->
<!--            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
<!--                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
<!--                 stroke-linejoin="round" class="feather feather-bar-chart-2 align-text-bottom "-->
<!--                 aria-hidden="true">-->
<!--                <line x1="18" y1="20" x2="18" y2="10"></line>-->
<!--                <line x1="12" y1="20" x2="12" y2="4"></line>-->
<!--                <line x1="6" y1="20" x2="6" y2="14"></line>-->
<!--            </svg>-->
<!--            Reports-->
<!--        </a>-->
<!--    </li>-->
<!--</ul>-->