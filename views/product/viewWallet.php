<?php
/** @var array $wallet */
/** @var array $producer */
foreach ($wallet as $key => $value)
    if(empty($value))
        $wallet[$key] = '-';
?>


<div class="container mx-5 my-3">
    <div class="row h-75" >
        <div class="col h-100">
            <div id="carouselExampleIndicators" class="carousel slide h-100" data-bs-ride="true">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner h-100">
                    <div class="carousel-item active  h-100">
                        <img src="/files/wallet/<?=$wallet['main_photo']?>" class="d-block w-100 h-100" alt="...">
                    </div>
                    <?php foreach ($wallet['photos'] as $photo):?>
                    <div class="carousel-item h-100">
                        <img src="/files/wallet/<?=$photo?>" class="d-block w-100 h-100" alt="...">
                    </div>
                    <?php endforeach;?>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next text-black" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
<!--            <img src="/files/wallet/--><?//=$wallet['main_photo']?><!--" class="img-fluid">-->
        </div>

        <div class="col bg-light rounded-5 h-100">
            <div class="col-4 mx-auto mt-3">
                <img src="/files/producer/<?= $producer['photo'] ?>" class="img-fluid card-img-top">
            </div>
            <h1 class="text-center my-2">
                <?= $wallet['name'] ?>
            </h1>

            <div class="text-secondary text-center mx-2 "><?= $wallet['description'] ?></div>
            <div class="row mt-5 align-items-end  ">
                <div class="col fs-5">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item bg-transparent list-group-item-action">
                        <div class="row">
                            <div class="col-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                                     class="bi bi-coin mb-0" viewBox="0 0 16 16">
                                    <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z"/>
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/>
                                </svg>
                            </div>
                            <div class="col-6">
                                <strong class="fs-4 text">$ <?=$wallet['price']?></strong>
                            </div>
                            <div class="col-4">
                                <button href="#" class="btn btn-success rounded-3 addToCart" id="Wallet" value="<?=$wallet['id']?>" >Add to cart</button>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item bg-transparent list-group-item-action">
                        <?php if($wallet['quantity_on_stock']>0):?>
                        <span>In stock (<span class="qty-text"><?=$wallet['quantity_on_stock']?></span> left)</span>
                        <?php else:?>
                        <span>Not in stock</span>
                        <?php endif;?>
                    </li>
                    <li class="list-group-item bg-transparent my-auto list-group-item-action">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modalInfo" class="text-decoration-none text-dark">
                            <div class="row">
                                <div class="col">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                                         class="bi bi-info-circle me-5" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                    </svg>
                                    Tech specs
                                </div>
                                <div class="col-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                                         class="bi bi-plus-lg me-auto" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                              d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                    </svg>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="list-group-item bg-transparent list-group-item-action">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2 align-text-bottom " aria-hidden="true"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                        Want to compare models?
                        <a href="/comparison/" class="btn btn-primary rounded-5">Compare</a>
                    </li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" id="modalInfo">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-2" id="exampleModalLabel">Technical specs</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container fs-6">
                    <div class="row">
                        <div class="col">
                            <strong>Supported coins</strong>
                            <p class="text-secondary">Over <?=$wallet['supported_coins']?></p>
                        </div>
                        <div class="col">
                            <strong>Warranty</strong>
                            <p class="text-secondary"><?=$wallet['warranty']?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <strong>Connectivity</strong>
                            <ul class="list-group list-group-flush">
                                <?php foreach ($wallet['connection'] as $item):?>
                                <li class="list-group-item bg-transparent list-group-item-action w-50"><?=$item?></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <div class="col">
                            <strong>Compatibility</strong>
                            <ul class="list-group list-group-flush">
                                <?php foreach ($wallet['compatibility'] as $item):?>
                                    <li class="list-group-item bg-transparent list-group-item-action w-50"><?=$item?></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <strong>Size (in mm)</strong>
                            <p class="text-secondary"><?=$wallet['size']?></p>
                        </div>
                        <div class="col">
                            <strong>Weight (in g)</strong>
                            <p class="text-secondary"><?=$wallet['weight']?></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>