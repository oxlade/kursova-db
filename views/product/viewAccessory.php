<?php
/** @var array $accessory */
/** @var array $producer */

?>


<div class="container mx-5 my-3">
    <div class="row h-60">
        <div class="col h-100">
            <div id="carouselExampleIndicators" class="carousel slide h-100" data-bs-ride="true">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner h-100">
                    <div class="carousel-item active  h-100">
                        <img src="/files/accessory/<?=$accessory['main_photo']?>" class="d-block w-100 h-100" alt="...">
                    </div>
                    <?php foreach ($accessory['photos'] as $photo):?>
                        <div class="carousel-item h-100">
                            <img src="/files/accessory/<?=$photo?>" class="d-block w-100 h-100" alt="...">
                        </div>
                    <?php endforeach;?>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next text-black" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                    <span class="carousel-control-next-icon " aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
        <div class="col bg-light rounded-5 h-100">
            <div class="col-4 mx-auto mt-3">
                <img src="/files/producer/<?= $producer['photo'] ?>" class="img-fluid card-img-top">
            </div>
            <h1 class="text-center my-2">
                <?= $accessory['name'] ?>
            </h1>

            <div class="text-secondary text-center mx-2 "><?= $accessory['description'] ?></div>
            <div class="row mt-5 mh-100 h-100 ">
                <div class="col fs-5">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item bg-transparent list-group-item-action">
                        <div class="row">
                            <div class="col-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                                     class="bi bi-coin mb-0" viewBox="0 0 16 16">
                                    <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z"/>
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/>
                                </svg>
                            </div>
                            <div class="col-6">
                                <strong class="fs-4 text">$ <?=$accessory['price']?></strong>
                            </div>
                            <div class="col-4">
                                <button href="#" class="btn btn-success rounded-3 addToCart" id="Accessory" value="<?=$accessory['id']?>" >Add to cart</button>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item bg-transparent list-group-item-action">
                        <?php if($accessory['quantity_on_stock']>0):?>
                        <span>In stock (<?=$accessory['quantity_on_stock']?> left)</span>
                        <?php else:?>
                        <span>Not in stock</span>
                        <?php endif;?>
                    </li>
                    <li class="list-group-item bg-transparent list-group-item-action">
                        Want to compare models?
                        <a href="/comparison/" class="btn btn-success rounded-5">Compare</a>
                    </li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>


