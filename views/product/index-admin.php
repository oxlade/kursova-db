<?php
use \models\User;

?>
<h1 class="my-2 text-center">List of products</h1>
<?php if (User::isAdmin()): ?>
    <div class="text-center">
        <a href="/product/addWallet" class="btn btn-success btn-lg w-25 mb-3">Add wallet</a>
        <a href="/product/addAccessory" class="btn btn-success btn-lg w-25 mb-3">Add accessory</a>
    </div>
<?php endif; ?>