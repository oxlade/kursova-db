<?php
/** @var array $errors */
/** @var array $producers */
/** @var array $model */
/** @var int|null $producer_id */
$connections = ['USB-A', 'USB-C', 'USB Micro-B', 'QR Code', 'Bluetooth'];
$compatibilities = ['Windows', 'Apple MacOS', 'Linux', 'Android', 'Apple iOS'];
\core\Core::getInstance()->pageParams['title'] = 'Wallet adding';
?>
<h1 class="text-center my-2">Wallet adding</h1>
<main class="form-add m-auto">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="name" class="form-label">Wallet`s name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name..."
                   value="<?= $model['name'] ?>">
            <?php if (!empty($errors['name'])): ?>
                <div id="nameHelp" class="form-text error"><?= $errors['name'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="producer_id" class="form-label">Wallet`s producer</label>
            <select name="producer_id" id="producer_id" class="form-control form-select">
                <?php foreach ($producers as $producer): ?>
                    <option value="<?= $producer['id'] ?>" <?php if($producer_id==$producer['id']) echo 'selected';?>><?= $producer['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Wallet`s price</label>
            <input type="number" class="form-control" id="price" name="price" placeholder="Price..." min="1"
                   value="<?= $model['price'] ?>">
            <?php if (!empty($errors['price'])): ?>
                <div id="priceHelp" class="form-text error"><?= $errors['price'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="quantity_on_stock" class="form-label">Quantity of wallets in stock</label>
            <input type="number" class="form-control" id="quantity_on_stock" name="quantity_on_stock"
                   placeholder="Quantity..." min="0" value="<?= $model['quantity_on_stock'] ?>">
            <?php if (!empty($errors['quantity_on_stock'])): ?>
                <div id="quantityHelp" class="form-text error"><?= $errors['quantity_on_stock'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="main_feature" class="form-label">Wallet`s main feature</label>
            <input type="text" class="form-control" id="main_feature" name="main_feature"
                   placeholder="Main feature..." value="<?= $model['main_feature'] ?>">
            <?php if (!empty($errors['main_feature'])): ?>
                <div id="featureHelp" class="form-text error"><?= $errors['main_feature'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Wallet`s description</label>
            <textarea class="form-control editor" id="description" name="description"
                      placeholder="Description..."><?= $model['description'] ?></textarea>
            <?php if (!empty($errors['description'])): ?>
                <div id="descriptionHelp" class="form-text error"><?= $errors['description'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="supported_coins" class="form-label">Wallet`s supported coins</label>
            <input type="number" class="form-control" id="supported_coins" name="supported_coins"
                   placeholder="Coins..." min="1" value="<?= $model['supported_coins'] ?>">
            <?php if (!empty($errors['supported_coins'])): ?>
                <div id="supportedCoinsHelp" class="form-text error"><?= $errors['supported_coins'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="connection" class="form-label">Wallet`s connection type</label>
            <select multiple name="connection[]" id="connection" class="form-control">
                <?php foreach ($connections as $connection): ?>
                    <option value="<?= $connection ?>" <?php if(isset($model['connection'])) if(in_array($connection,$model['connection'])) echo 'selected';?>><?= $connection ?></option>
                <?php endforeach; ?>
            </select>
            <?php if (!empty($errors['compatibility'])): ?>
                <div id="connectionHelp" class="form-text error"><?= $errors['connection'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="compatibility" class="form-label">Wallet`s compatibility</label>
            <select multiple name="compatibility[]" id="compatibility" class="form-control">
                <?php foreach ($compatibilities as $compatibility): ?>
                    <option value="<?= $compatibility ?>" <?php if(isset($model['compatibility'])) if(in_array($compatibility,$model['compatibility'])) echo 'selected';?>><?= $compatibility ?></option>
                <?php endforeach; ?>
            </select>
            <?php if (!empty($errors['compatibility'])): ?>
                <div id="compatibilityHelp" class="form-text error"><?= $errors['compatibility'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="warranty" class="form-label">Wallet`s warranty</label>
            <input type="text" class="form-control" id="warranty" name="warranty" placeholder="Warranty..."
                   value="<?= $model['warranty'] ?>">
            <?php if (!empty($errors['warranty'])): ?>
                <div id="warrantyHelp" class="form-text error"><?= $errors['warranty'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="height" class="form-label">Wallet`s size (in mm)</label>
            <div class="input-group">
                <label for="height"><span class="input-group-text">Height</span></label>
                <input type="number" class="form-control me-1" id="height" name="height"
                       value="<?= $model['height'] ?>">
                <label for="width"><span class="input-group-text">Width</span></label>
                <input type="number" class="form-control me-1" id="width" name="width" value="<?= $model['width'] ?>">
                <label for="length"><span class="input-group-text">Length</span></label>
                <input type="number" class="form-control" id="length" name="length" value="<?= $model['length'] ?>">
            </div>
            <?php if (!empty($errors['size'])): ?>
                <div id="sizeHelp" class="form-text error"><?= $errors['size'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="weight" class="form-label">Wallet`s weight (in g)</label>
            <input type="number" class="form-control" id="weight" name="weight"
                   placeholder="Weight..." min="1" value="<?= $model['weight'] ?>">
            <?php if (!empty($errors['weight'])): ?>
                <div id="weightHelp" class="form-text error"><?= $errors['weight'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="visible" class="form-label w-100">Visible</label>
            <select name="visible" id="visible" class="form-control">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <div class="row col-12 mx-auto mb-3 ElemforInsertProductbtn w-100 h-30">
            <div class="imageBasket row row-cols-1 row-cols-md-3 w-100 mh-15"></div>
            <div class="col-6 mx-auto form-floating mb-1 d-flex ElemforDeleteProductbtn w-100">
                <input style="height: 70px; padding-top: 36px;padding-left: 30px" multiple type="file"
                       class="form-control InputProductJs w-75" id="floatingIMG" name="files[]"  accept="image/png">
                <label class="ms-2" for="floatingIMG">Картинка:</label>
                <div class="col-1 mx-auto ms-1">
                    <a class="btnForResetAddProd btn btn-close d-flex justify-content-center" style="min-width: 50px"></a>
                </div>
            </div>
            <?php if (!empty($errors['main_photo'])): ?>
                <div class="form-text mb-3 fw-normal text-center alert alert-danger">
                    <?= $errors['main_photo'] ?>
                </div>
            <?php endif; ?>
            <hr>
        </div>
        <div class="row-cols-1">
            <button class="btn btn-success">Add</button>
        </div>
    </form>
</main>

<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('.editor'))
        .catch(error => {
            console.error(error);
        });
</script>

