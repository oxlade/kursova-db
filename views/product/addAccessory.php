<?php
/** @var array $errors */
/** @var array $producers */
/** @var array $model */
/** @var int|null $producer_id */
\core\Core::getInstance()->pageParams['title'] = 'Accessory adding';
?>
<h1 class="text-center my-2">Accessory adding</h1>
<main class="form-add m-auto">
    <form action="" method="post" enctype="multipart/form-data" >
        <div class="mb-3">
            <label for="name" class="form-label">Accessory name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Accessory..." value="<?=$model['name']?>">
            <?php if (!empty($errors['name'])): ?>
                <div id="nameHelp" class="form-text error"><?= $errors['name'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="producer_id" class="form-label">Accessory`s producer</label>
            <select name="producer_id" id="producer_id" class="form-control form-select">
                <?php foreach ($producers as $producer): ?>
                    <option value="<?= $producer['id'] ?>" <?php if($producer_id==$producer['id']) echo 'selected';?>><?= $producer['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Accessory`s price</label>
            <input type="number" class="form-control" id="price" name="price" placeholder="Price..." min="1" value="<?=$model['price']?>">
            <?php if (!empty($errors['price'])): ?>
                <div id="priceHelp" class="form-text error"><?= $errors['price'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="quantity_on_stock" class="form-label">Quantity of accessories in stock</label>
            <input type="number" class="form-control" id="quantity_on_stock" name="quantity_on_stock"
                   placeholder="Quantity..." min="0" value="<?=$model['quantity_on_stock']?>">
            <?php if (!empty($errors['quantity_on_stock'])): ?>
                <div id="priceHelp" class="form-text error"><?= $errors['quantity_on_stock'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Accessory`s description</label>
            <textarea class="form-control editor" id="description" name="description" placeholder="Description..."><?=$model['quantity_on_stock']?></textarea>
            <?php if (!empty($errors['description'])): ?>
                <div id="priceHelp" class="form-text error"><?= $errors['description'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="universal" class="form-label w-100">Universality (suitable for other producers products)</label>
            <select name="universal" id="universal" class="form-control">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="visible" class="form-label w-100">Visible</label>
            <select name="visible" id="visible" class="form-control">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </div>
        <div class="row col-12 mx-auto mb-3 ElemforInsertProductbtn w-100 ">
            <div class="imageBasket row row-cols-1 row-cols-md-3 w-100 mh-15"></div>
            <div class="col-6 mx-auto form-floating mb-1 d-flex ElemforDeleteProductbtn w-100 align-content-center">
                <input style="height: 70px;" multiple type="file"
                       class="form-control InputProductJs w-100 p-5 m-auto" id="floatingIMG" name="files[]"  accept="image/png">
                <label class="ms-2" for="floatingIMG">Picture:</label>
                <div class="col-1 mx-auto ms-1">
                    <a class="btnForResetAddProd btn btn-close d-flex justify-content-center" style="min-width: 50px"></a>
                </div>
            </div>
            <?php if (!empty($errors['main_photo'])): ?>
                <div class="form-text mb-3 fw-normal text-center alert alert-danger">
                    <?= $errors['main_photo'] ?>
                </div>
            <?php endif; ?>
            <hr>
        </div>
        <div class="row-cols-1">
            <button class="btn btn-success">Add</button>
        </div>
    </form>
</main>

<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '.editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
