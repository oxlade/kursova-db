<?php
\core\Core::getInstance()->pageParams['title'] = 'Main page';
?>
<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>
<main>
    <div id="myCarousel" class="carousel slide h-50" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-label="Slide 1"
                    aria-current="true"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"
                    class=""></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"
                    class=""></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <!--                <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#777"></rect></svg>-->
                <!--                <img src="https://www.safepal.com/assets/img/hero-mobile.svg" class="w-100 h-100" alt="">-->
                <img src="https://cryptoadventure.com/wp-content/uploads/2021/08/cryptoadventure.png"
                     style="transform: scaleX(-1);" class="w-100 h-100" alt="">
                <div class="container">
                    <div class="carousel-caption text-start text-light">
                        <h1><span class="text-warning">Start Your Own</span> <br>Crypto Adventure</h1>
                        <!--                        <p>Some representative placeholder content for the first slide of the carousel.</p>-->
                        <p><a class="btn btn-lg btn-primary" href="/user/register">Sign up today</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item bg-darkblue-radial">
                <img src="https://www.safepal.com/assets/img/stand-out-img-center.svg"
                     class="h-100 w-100" alt="">
                <div class="container k">
                    <div class="carousel-caption text-white text-end">
                        <h1><span class="text-success">Choose the safe place</span><br> for your coins</h1>
                        <p>Protect & manage Bitcoin, Ethereum and thousands of other<br> digital assets with your pick
                            from our hardware wallets.</p>
                        <p><a class="btn btn-lg btn-success" href="#">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item ">
                <img src="https://dailyhodl.com/wp-content/uploads/2022/02/gobbling-unloading-firm.jpg" class="w-100 h-100"
                     alt="">

                <div class="container">
                    <div class="carousel-caption text-start text-white ">
                        <h1><span class="text-danger">Get more</span><br> from your wallet.</h1>
                        <p>Our wallets don't just safeguard your crypto,<br> they also create unique possibilities to grow
                            your wealth.</p>
                        <p><a class="btn btn-lg btn-primary" href="#">Browse gallery</a></p>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing text-center">

        <!-- Three columns of text below the carousel -->
<!--        <div class="row">-->
<!--            <div class="col-lg-4">-->
<!--                <svg class="bd-placeholder-img rounded-circle" width="140" height="140"-->
<!--                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 140x140"-->
<!--                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>-->
<!--                    <rect width="100%" height="100%" fill="#777"></rect>-->
<!--                    <text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>-->
<!--                </svg>-->
<!---->
<!--                <h2>Heading</h2>-->
<!--                <p>Some representative placeholder content for the three columns of text below the carousel. This is the-->
<!--                    first column.</p>-->
<!--                <p><a class="btn btn-secondary" href="#">View details »</a></p>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <svg class="bd-placeholder-img rounded-circle" width="140" height="140"-->
<!--                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 140x140"-->
<!--                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>-->
<!--                    <rect width="100%" height="100%" fill="#777"></rect>-->
<!--                    <text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>-->
<!--                </svg>-->
<!---->
<!--                <h2>Heading</h2>-->
<!--                <p>Another exciting bit of representative placeholder content. This time, we've moved on to the second-->
<!--                    column.</p>-->
<!--                <p><a class="btn btn-secondary" href="#">View details »</a></p>-->
<!--            </div>-->
<!--            <div class="col-lg-4">-->
<!--                <svg class="bd-placeholder-img rounded-circle" width="140" height="140"-->
<!--                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 140x140"-->
<!--                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>-->
<!--                    <rect width="100%" height="100%" fill="#777"></rect>-->
<!--                    <text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>-->
<!--                </svg>-->
<!---->
<!--                <h2>Heading</h2>-->
<!--                <p>And lastly this, the third column of representative placeholder content.</p>-->
<!--                <p><a class="btn btn-secondary" href="#">View details »</a></p>-->
<!--            </div>-->
<!--        </div>-->


        <!-- START THE FEATURETTES -->
        <hr class="featurette-divider my-5">

        <div class="row align-items-center featurette my-2 h-55">
            <div class="col-md-7">
                <h2 class="featurette-heading text-black display-3 fw-bolder">What is a<br> <span class="text-blue-red">hardware wallet?</span></h2>
                <p class="lead fs-4">A hardware wallet is a cryptocurrency wallet which stores the user's private keys
                    (critical piece of information used to authorise outgoing transactions on the blockchain network)
                    in a secure hardware device.</p>
            </div>
            <div class="col-md-5 order-md-1 bg-blue-red-linear rounded-4 py-2">
                <img src="https://cdn.shopify.com/s/files/1/2974/4858/products/Staxmain.png?v=1670332896&width=600"
                     class="w-75 h-100" alt="">
            </div>
        </div>
        <hr class="featurette-divider my-5">

        <div class="row featurette align-items-center my-2 h-55">
            <div class="col-md-7 order-md-2">
                <h1 class="featurette-heading text-green display-3 fw-bolder">Your data.
                     <span class="text-black"><br>Your control.</span></h1>
                <p class="lead fs-4">Online exchanges and wallets are vulnerable to online security attacks and data leaks.
                    Offline hardware wallets protect your assets and identity by removing online access to your data.</p>
            </div>
            <div class="col-md-5 bg-darkgreen-linear rounded-4 order-md-1" >
                <img src="https://trezor.io/_next/image?url=%2F_next%2Fstatic%2Fimage%2Fpublic%2Fimages%2Fdesign%2FPrivacySecurity%402x.3caaf095a7d33deab86f5188d7064d07.png&w=1920&q=75"
                     class="w-100 h-100" alt="">
            </div>
        </div>

        <hr class="featurette-divider my-5">

        <div class="row align-items-center featurette my-2 h-55">
            <div class="col-md-7 ">
                <h2 class="featurette-heading display-3 fw-bolder text-darkblue">One place<br>
                     <span class="text-black">for all your crypto needs</span></h2>
                <p class="lead fs-4"><strong>Buy</strong> Bitcoin and other cryptocurrencies – the simple, safe, smart way.<br>
                    <strong>Grow</strong> your crypto assets easily – all on one secure platform.<br>
                    <strong>Exchange</strong> crypto in seconds to get the most out of your crypto assets.<br></p>
            </div>
            <div class="col-md-5  bg-darkblue-linear rounded-4 py-2">
                <img src="https://www.ledger.com/wp-content/uploads/2022/04/Exchange-1.png"
                     class="w-75 h-100" alt="">
            </div>
        </div>



        <hr class="featurette-divider my-5">

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->



</main>