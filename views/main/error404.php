<!--<link rel="stylesheet" href="/themes/light/css/style-error.css" type="text/css">-->
<main id="main" class="ldg-container">
    <section class="my-row flexbox">
        <div class="my-row align-left b-title-master">
            <h1 class="">Oops!</h1>

            <p>We couldn't find the content you were looking for. It might be lost in space, floating amongst stars.</p>
            <p>But don't worry, you're always safe with us, even on our 404 page.</p>
            <br>
            <a href="/" title="" class="go-back-button">
                Go back home <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-90deg-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M1.146 4.854a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H12.5A2.5 2.5 0 0 1 15 6.5v8a.5.5 0 0 1-1 0v-8A1.5 1.5 0 0 0 12.5 5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4z"/>
                </svg>
            </a>
        </div>
        <div class="my-row">
            <img src="https://www.ledger.com/wp-content/uploads/2019/05/404.png" alt="" class="error-image">
        </div>
    </section>
</main>



