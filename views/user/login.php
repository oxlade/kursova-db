<?php
/** @var string|null $error */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Login';
?>
<h1 class="h3 mt-2 mb-3 fw-normal text-center">User verification</h1>
<?php if(!empty($error)) : ?>
<div class="message error text-center mb-2">
    <?=$error?>
</div>
<?php endif;?>
<main class="form-signin w-100 m-auto">
    <form method="post" action="">
        <div class="form-floating">
            <input type="email" class="form-control"  name="login" id="login" value="<?=$model['login']?>" placeholder="name@example.com">
            <label for="login">Email address</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" name="password" id="password" value="<?=$model['password']?>" placeholder="Password">
            <label for="password">Password</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    </form>
</main>