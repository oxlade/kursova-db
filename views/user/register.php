<?php
/** @var array $errors */
/** @var array $model */
\core\Core::getInstance()->pageParams['title'] = 'Registration';
?>
<h1 class="h3 mt-2 mb-3 fw-normal text-center">New user registration</h1>
<main class="form-signin w-100 m-auto">
    <form method="post" action="">
        <div class="mb-3">
            <label for="login">Login</label>
            <input type="email" name="login" id="login" class="form-control" value="<?= $model['login'] ?>" placeholder="Email..." aria-describedby="emailHelp">
            <?php if (!empty($errors['login'])): ?>
                <div id="emailHelp" class="form-text error"><?= $errors['login'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="first_name">First name</label>
            <input type="text" name="first_name" id="first_name" class="form-control" value="<?= $model['first_name'] ?>"
                   placeholder="First name..." aria-describedby="firstNameHelp">
            <?php if (!empty($errors['first_name'])): ?>
                <div id="firstNameHelp" class="form-text error"><?= $errors['first_name'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="last_name">Last name</label>
            <input type="text" name="last_name" id="last_name" class="form-control" value="<?= $model['last_name'] ?>"
                   placeholder="Last name..." aria-describedby="LastNameHelp">
            <?php if (!empty($errors['last_name'])): ?>
                <div id="LastNameHelp" class="form-text error"><?= $errors['last_name'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" value="<?= $model['password'] ?>"
                   placeholder="Password..." aria-describedby="passwordHelp">
            <?php if (!empty($errors['password'])): ?>
                <div id="passwordHelp" class="form-text error"><?= $errors['password'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="password2">Repeat password</label>
            <input type="password" name="password2" id="password2" class="form-control" value="<?= $model['password'] ?>"
                   placeholder="Password...">
        </div>
        <div>
            <button class="btn btn-primary w-100">Register</button>
        </div>
    </form>
</main>