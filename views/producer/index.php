<?php
/** @var array $rows */

use \models\User;
\core\Core::getInstance()->pageParams['title'] = 'Producers';
?>
<h1 class="my-2 text-center">List of producers</h1>
<div class="row row-cols-1 row-cols-md-3 g-4 producers-list">
    <?php foreach ($rows as $row): ?>
        <div class="col">
            <a href="/producer/view/<?= $row['id'] ?>">
            <div class="card">
                <img src="/files/producer/<?= $row['photo'] ?>" class="card-img-top image-producer" alt="...">
                <div class="card-body">
                    <h4 class="card-title text-center my-2"><?= $row['name'] ?></h4>
                </div>
            </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
