<?php
/** @var array $rows */

use \models\User;

?>
<h1 class="my-2 text-center">List of producers</h1>
<?php if (User::isAdmin()): ?>
    <div class="text-center">
        <a href="/producer/add" class="btn btn-success btn-lg w-25 mb-3">Add producer</a>
    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-3 g-4 producers-list">
    <?php foreach ($rows as $row): ?>
        <div class="col">
            <a href="/producer/view/<?= $row['id'] ?>">
                <div class="card">
                    <img src="/files/producer/<?= $row['photo'] ?>" class="card-img-top image-producer" alt="...">
                    <div class="card-body">
                        <h4 class="card-title text-center my-3"><?= $row['name'] ?></h4>
                        <a href="/producer/edit/<?= $row['id'] ?>" class="btn btn-warning w-100 mb-2">Edit</a>
                        <a href="#" class="btn btn-danger w-100" data-bs-toggle="modal"
                           data-bs-target="#modalDelete<?= $row['id'] ?>">Delete</a>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<?php foreach ($rows as $row): ?>
    <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" id="modalDelete<?= $row['id'] ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">You really want to delete <?=$row['name']?> producer?</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Once you delete the producer, all his products will be removed from the shop!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close
                    </button>
                    <a href="/producer/delete/<?= $row['id'] ?>" class="btn btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

