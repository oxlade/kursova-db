<?php
/** @var array $producer */
/** @var array $errors */
?>
<h1 class="text-center my-3">Producer editing </h1>
<main class="form-add w-100 m-auto">
    <div class="col-4 mx-auto">
        <img src="/files/producer/<?=$producer['photo']?>" class="img-fluid card-img-top">
    </div>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="name" class="form-label">Producer name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?=$producer['name']?>">
            <?php if (!empty($errors['name'])): ?>
                <div id="nameHelp" class="form-text error"><?= $errors['name'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="info" class="form-label">Producer`s information</label>
            <textarea class="form-control editor" id="info" name="info"
                      placeholder="Info..."></textarea>
            <?php if (!empty($errors['info'])): ?>
                <div id="descriptionHelp" class="form-text error"><?= $errors['info'] ?> </div>
            <?php endif; ?>
        </div>
        <div class="mb-3">
            <label for="file" class="form-label">Producer photo (logo)</label>
            <input type="file" class="form-control" id="file" name="file" accept="image/png">
        </div>
        <div class="row-cols-1">
            <button class="btn btn-success">Save</button>
        </div>
        <div class="row-cols-1 text-center my-2">
            <a href="/producer/" class="btn btn-dark w-50">Discard changes</a>
        </div>
    </form>
</main>
<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('.editor'))
        .catch(error => {
            console.error(error);
        });
</script>