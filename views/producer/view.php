<?php
/** @var array $producer */
/** @var array $products */
use models\User;

?>
<h1 class="text-center my-2"><?= $producer['name'] ?></h1>
<?php if (User::isAdmin()): ?>
    <div class="text-center">
        <a href="/product/addWallet/<?= $producer['id'] ?>" class="btn btn-success btn-lg w-25 mb-3 me-1">Add wallet</a>
        <a href="/product/addAccessory/<?= $producer['id'] ?>" class="btn btn-success btn-lg w-25 mb-3 ms-1">Add
            accessory</a>
    </div>
<?php endif; ?>
<div id="main" class="ldg-container">
    <div class="my-row flexbox mb-2">
        <div class="my-row me-1">
            <img src="/files/producer/<?= $producer['photo'] ?>" alt="" class="error-image ">
        </div>
        <div class="my-row rounded-5 bg-light my-3 ms-1">
            <?= $producer['info'] ?>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-md-4 g-4 producers-list">
        <?php foreach ($products as $row): ?>
            <div class="col">
                <a href="/product/view<?php if(array_key_exists('supported_coins', $row)) echo 'Wallet'; else echo 'Accessory';?>/<?= $row['id'] ?>">
                    <div class="card">
                        <img src="https://www.ledger.com/wp-content/uploads/2019/05/404.png"
                             class="card-img-top image-producer" alt="...">
                        <div class="card-body">
                            <h4 class="card-title text-center my-3"><?= $row['name'] ?></h4>
                            <?php if (User::isAdmin()): ?>
                                <a href="/producer/edit/<?= $row['id'] ?>" class="btn btn-warning w-100 mb-2">Edit</a>
                                <a href="#" class="btn btn-danger w-100" data-bs-toggle="modal"
                                   data-bs-target="#modalDelete<?= $row['id'] ?>">Delete</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>