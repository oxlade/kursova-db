<?php
include('error_reporting.php');
include('config/database.php');
include('config/params.php');
spl_autoload_register(function ($className) {
    $className = str_replace("\\", "/", $className);
    $path = $className . '.php';
    if (file_exists($path))
        require($path);
});
$core = core\Core::getInstance();
$core->Initialize();
$core->Run();
$core->Done();
