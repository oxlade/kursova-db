<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\Accessory;
use models\Producer;
use models\User;
use models\Wallet;

class ProducerController extends Controller
{
    public function indexAction()
    {
        $rows = Producer::getProducers();
        $viewPath = null;
        if (User::isAdmin())
            $viewPath = 'views/producer/index-admin.php';
        return $this->render($viewPath, [
            'rows' => $rows
        ]);
    }

    public function addAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        if (Core::getInstance()->requestMethod === "POST") {
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Producer name cannot be empty!';
            if (Producer::isNameExists($_POST['name']))
                $errors['name'] = 'This producer is already in our system!';
            if (count($errors) > 0)
                return $this->render(null, [
                    'errors' => $errors
                ]);
            else {
                Producer::add($_POST['name'], $_FILES['file']['tmp_name']);
                $this->redirect('/producer/');
            }
        }
        return $this->render();
    }

    public function deleteAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $producer = Producer::getProducerById($id);
            $filePath = 'files/producer/' . $producer['photo'];
            if (is_file($filePath))
                unlink($filePath);
            Producer::delete($id);
            $this->redirect('/producer/');

            return $this->render(null, [
                'producer' => $producer
            ]);
        } else
            return $this->error(403);
    }

    public function editAction($params)
    {
        $id = intval($params[0]);
        if (!User::isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $producer = Producer::getProducerById($id);
            if (Core::getInstance()->requestMethod === "POST") {
                $errors = [];
                if (empty($_POST['name']))
                    $errors['name'] = 'Producer name cannot be empty!';
                if (Producer::isNameExists($_POST['name']) && $_POST['name'] != $producer['name'])
                    $errors['name'] = 'This producer is already in our system!';
                if (count($errors) > 0)
                    return $this->render(null, [
                        'errors' => $errors,
                        'producer' => $producer
                    ]);
                else {
                    Producer::update($id, $_POST['name'], $_POST['info']);
                    if (!empty($_FILES['file']['tmp_name']))
                        Producer::changePhoto($id, $_FILES['file']['tmp_name']);
                    $this->redirect('/producer/');
                }
            }
            return $this->render(null, [
                'producer' => $producer
            ]);
        } else
            return $this->error(403);
    }

    public function viewAction($params)
    {
        $id = intval($params[0]);
        if ($id > 0) {
            $producer = Producer::getProducerById($id);
            if (!empty($producer)) {
                $wallets = Wallet::getWalletsByProducerId($id);
                $countWallets = count($wallets);
                if($countWallets >= 4){
                    $wallets = array_slice($wallets,0,4);
                }
                else{
                    $accessories = Accessory::getAccessoriesByProducerId($id);
                    if(count($accessories)>0){
                        if(count($accessories)+$countWallets > 4)
                            $accessories = array_slice($accessories,0,4-$countWallets);
                        foreach ($accessories as $item)
                            $wallets[] = $item;
                    }
                }
                return $this->render(null, [
                    'producer' => $producer,
                    'products' => $wallets
                ]);
            } else
                return $this->error(404);
        }
        return $this->error(404);
    }
}