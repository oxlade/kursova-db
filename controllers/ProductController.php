<?php

namespace controllers;

use \core\Controller;
use core\Core;
use models\Accessory;
use models\Producer;
use models\User;
use models\Wallet;

class ProductController extends Controller
{
    public function indexAction()
    {
        $viewPath = null;
        if (User::isAdmin())
            $viewPath = 'views/product/index-admin.php';
        $producers = Producer::getProducers();
        if (Core::getInstance()->requestMethod === "POST") {
            if ($_POST['product_type'] == 'wallet')
                $products = Wallet::getWallets();
            elseif ($_POST['product_type'] == 'accessory')
                $products = Accessory::getAccessories();
            elseif ($_POST['product_type'] == 'all') {
                $products = Wallet::getWallets();
                $accesories = Accessory::getAccessories();
                foreach ($accesories as $item)
                    $products[] = $item;
            }

            return $this->render($viewPath, [
                'products' => $products,
                'producers' => $producers
            ]);
        } else {
            $products = Wallet::getWallets();
            $accesories = Accessory::getAccessories();
            foreach ($accesories as $item)
                $products[] = $item;

            return $this->render($viewPath, [
                'products' => $products,
                'producers' => $producers
            ]);
        }

    }

    public function addWalletAction($params)
    {
        $producer_id = intval($params[0]);
        if (empty($producer_id))
            $producer_id = null;
        if (!User::isAdmin())
            return $this->error(403);
        $producers = Producer::getProducers();
        if (Core::getInstance()->requestMethod === "POST") {
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Wallet`s name cannot be empty!';
            if (Wallet::isNameExists($_POST['name']))
                $errors['name'] = 'This wallet is already in our system!';
            if ($_POST['price'] < 1)
                $errors['price'] = 'Wallet`s price is too low!';
            if (empty($_POST['price']))
                $errors['price'] = 'Wallet`s price cannot be empty!';
            if ($_POST['quantity_on_stock'] < 0)
                $errors['quantity_on_stock'] = 'Wallet`s price is too low!';
            if (empty($_POST['quantity_on_stock']))
                $errors['quantity_on_stock'] = 'Wallet`s quantity cannot be empty!';
            if (empty($_POST['description']))
                $errors['description'] = 'Wallet`s description cannot be empty!';
            if ($_POST['supported_coins'] < 1 && !empty($_POST['supported_coins']))
                $errors['supported_coins'] = 'Coins number is too low!';
            if ($_POST['weight'] < 5 && !empty($_POST['weight']))
                $errors['weight'] = 'Wallet`s weight is too low!';
            if (!is_array($_POST['connection']))
                $errors['connection'] = 'Incorrect input format!';
            if (!is_array($_POST['compatibility']))
                $errors['compatibility'] = 'Incorrect input format!';
            if ($_POST['height'] < 1 && !empty($_POST['height']))
                $errors['height'] = 'Incorrect!';
            if ($_POST['length'] < 1 && !empty($_POST['length']))
                $errors['length'] = 'Incorrect!';
            if ($_POST['width'] < 1 && !empty($_POST['width']))
                $errors['width'] = 'Incorrect!';
            if (empty($_POST['main_photo']))
                $errors['main_photo'] = "Main photo hasn't been chosen!";

            if (count($errors) > 0) {
                if (isset($errors['length']) || isset($errors['width']) || isset($errors['height']))
                    $errors['size'] = 'Incorrect input values!';
                $keys = array_keys($errors);
                foreach ($_POST as $key => $value)
                    if (!in_array($key, $keys))
                        $model[$key] = $value;

                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model,
                    'producers' => $producers,
                    'producer_id' => $producer_id
                ]);
            } else {
                Wallet::add($_POST,$_FILES);
                $this->redirect('/product/');
            }

        } else {
            return $this->render(null, [
                'producers' => $producers,
                'producer_id' => $producer_id
            ]);
        }
    }

    public function addAccessoryAction($params)
    {
        $producer_id = intval($params[0]);
        if (empty($producer_id))
            $producer_id = null;
        if (!User::isAdmin())
            return $this->error(403);
        $producers = Producer::getProducers();
        if (Core::getInstance()->requestMethod === "POST") {
            $errors = [];
            if (empty($_POST['name']))
                $errors['name'] = 'Accessory name cannot be empty!';
            if (Accessory::isNameExists($_POST['name']))
                $errors['name'] = 'This accessory is already in our system!';
            if ($_POST['price'] < 1)
                $errors['price'] = 'Accessory`s price is too low!';
            if (empty($_POST['price']))
                $errors['price'] = 'Accessory`s price cannot be empty!';
            if ($_POST['quantity_on_stock'] < 0)
                $errors['quantity_on_stock'] = 'Accessory`s price is too low!';
            if (empty($_POST['quantity_on_stock']))
                $errors['quantity_on_stock'] = 'Accessory`s quantity cannot be empty!';
            if (empty($_POST['description']))
                $errors['description'] = 'Accessory`s description cannot be empty!';
            if (empty($_POST['main_photo']))
                $errors['main_photo'] = "Main photo hasn't been chosen!";
            if (count($errors) > 0) {
                $keys = array_keys($errors);
                foreach ($_POST as $key => $value)
                    if (!in_array($key, $keys))
                        $model[$key] = $value;

                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model,
                    'producers' => $producers,
                    'producer_id' => $producer_id
                ]);
            } else {
                Accessory::add($_POST, $_FILES);
                $this->redirect('/product/');
            }

        } else {
            return $this->render(null, [
                'producers' => $producers,
                'producer_id' => $producer_id
            ]);
        }
    }

    public function viewWalletAction($params)
    {
        $id = intval($params[0]);
        if ($id > 0) {
            $wallet = Wallet::getWalletById($id);
            if (!empty($wallet)) {
                $producer = Producer::getProducerById($wallet['producer_id']);
                return $this->render(null, [
                    'wallet' => $wallet,
                    'producer' => $producer
                ]);
            } else
                return $this->error(404);
        }
        return $this->error(404);
    }

    public function viewAccessoryAction($params)
    {
        $id = intval($params[0]);
        if ($id > 0) {
            $accessory = Accessory::getAccessoryById($id);
            if (!empty($accessory)) {
                $producer = Producer::getProducerById($accessory['producer_id']);
                return $this->render(null, [
                    'accessory' => $accessory,
                    'producer' => $producer
                ]);
            } else
                return $this->error(404);
        }
        return $this->error(404);
    }
}