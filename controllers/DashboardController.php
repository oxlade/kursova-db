<?php

namespace controllers;

use \core\Controller;
use models\Accessory;
use models\User;
use models\Wallet;

class DashboardController extends Controller
{
    public function indexAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        $customers = User::getCustomers();
        $products = Wallet::getWallets();
        $accessories = Accessory::getAccessories();
        foreach ($accessories as $key => $value)
            $products[] = $value;
        return $this->render(null,[
            'customers' => $customers,
            'products' => $products
        ]);
    }

    public function customersAction()
    {
        if (!User::isAdmin())
            return $this->error(403);
        $customers = User::getCustomers();
        return $this->render(null,[
            'customers' => $customers
        ]);
    }
}