<?php

namespace controllers;

use \core\Controller;
use models\Cart;

class CartController extends Controller
{
    public function indexAction(){
        $products = Cart::getCartProducts();
        return $this->render(null,[
            'products' => $products
        ]);
    }
    public function addWalletAction(){
        Cart::addWallet();
    }
    public function addAccessoryAction(){
        Cart::addAccessory();
    }
    public function updateAction(){
        Cart::update();
    }
    public function deleteAction(){
        Cart::delete();
    }

}