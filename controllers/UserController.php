<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\User;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        echo "User index action!";
    }

    public function registerAction()
    {
        if (User::isUserAuthenticated())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $errors = [];
            if (!filter_var($_POST['login'], FILTER_VALIDATE_EMAIL))
                $errors ['login'] = "Login input error!";
            if (User::isLoginExists($_POST['login']))
                $errors['login'] = "This login is already in our system!";
            if ($_POST['password'] != $_POST['password2'])
                $errors['password'] = "Passwords are not equal!";
            else
                if ($_POST['password'] == "")
                    $errors['password'] = "Password cannot be empty!";
            if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['first_name']))
                $errors['first_name'] = "First name input error! *Only letters and white space allowed";
            if (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['last_name']))
                $errors['last_name'] = "Last name input error! *Only letters and white space allowed";

            if (count($errors) > 0) {
                $keys = array_keys($errors);
                foreach ($_POST as $key => $value)
                    if (!in_array($key, $keys))
                        $model[$key] = $value;

                return $this->render(null, [
                    'errors' => $errors,
                    'model' => $model
                ]);
            } else {
                User::add($_POST['login'], $_POST['password'],
                    $_POST['first_name'], $_POST['last_name']);
                return $this->renderView('register-successful');
            }
        } else
            return $this->render();
    }

    public function loginAction()
    {
        if (User::isUserAuthenticated())
            $this->redirect('/');
        if (Core::getInstance()->requestMethod === 'POST') {
            $user = User::getUserByLoginAndPassword($_POST['login'], $_POST['password']);
            $error = null;
            if (empty($user))
                $error = "Incorrect login or password!";
            else {
                User::authenticate($user);
                $this->redirect('/');
            }
        }
        return $this->render(null, [
            'error' => $error
        ]);
    }

    public function logoutAction()
    {
        User::logoutUser();
        $this->redirect('/user/login');
    }

    public function dashboardAction(){
        if (!User::isAdmin())
            return $this->error(403);
//        Core::getInstance()->pageParams['title'] = 'Dashboard';
        return $this->render();
    }
}