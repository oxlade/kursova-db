<?php

namespace controllers;

use \core\Controller;
use core\Core;
use core\Utils;
use models\User;
use models\Wallet;

class ComparisonController extends Controller
{
    public function indexAction()
    {
        $comparisonElems = ['supported_coins', 'connection', 'compatibility', 'warranty', 'size', 'weight'];
        $rows = Wallet::getWallets();
        $wallets = $rows;
        $error = '';
        if (Core::getInstance()->requestMethod === "POST") {
            if($_POST['wallet1'] == $_POST['wallet2'])
                $error = 'You cannot compare the same models!';
            else {
                $wallet1 = Wallet::getWalletById($_POST['wallet1']);
                $wallet2 = Wallet::getWalletById($_POST['wallet2']);
                return $this->render(null, [
                    'wallet1' => $wallet1,
                    'wallet2' => $wallet2,
                    'wallets' => $wallets,
                    'comparisonElems' => $comparisonElems
                ]);
            }
        }
        $wallet1 = array_shift($rows);
        $wallet2 = array_shift($rows);
        return $this->render(null, [
            'wallet1' => $wallet1,
            'wallet2' => $wallet2,
            'wallets' => $wallets,
            'comparisonElems' => $comparisonElems,
            'error' => $error
        ]);

    }
}