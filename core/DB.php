<?php

namespace core;

/**
 * Class that is responsible for DB
 */
class DB
{
    protected $pdo;

    public function __construct($hostname, $login, $password, $database)
    {
        $this->pdo = new \PDO("mysql: host=${hostname};dbname=${database}",
            $login, $password);
    }

    public function select($tableName, $fieldsList = "*",
                           $conditionArray = null)
    {
        if (is_string($fieldsList))
            $fieldsListString = $fieldsList;
        elseif (is_array($fieldsList))
            $fieldsListString = implode(',', $fieldsList);
        $wherePartString = "";
        if (is_array($conditionArray)) {
            $parts = [];
            foreach ($conditionArray as $key => $value)
                $parts [] = "{$key} = :{$key}";
            $wherePartString = " WHERE " . implode(' AND ', $parts);
        }
        $res = $this->pdo->prepare("SELECT {$fieldsListString} 
            FROM {$tableName} {$wherePartString}");
        $ex = $res->execute($conditionArray);
        return $res->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function update($tableName, $newValues, $conditionArray)
    {
        $setParts = [];
        $paramsArray = [];
        foreach ($newValues as $key => $value) {
            $setParts [] = "{$key} = :set{$key}";
            $paramsArray ['set' . $key] = $value;
        }
        $setPartString = implode(',', $setParts);
        $whereParts = [];
        foreach ($conditionArray as $key => $value) {
            $whereParts [] = "{$key} = :{$key}";
            $paramsArray [$key] = $value;
        }
        $wherePartString = " WHERE " . implode(' AND ', $whereParts);
        $res = $this->pdo->prepare("UPDATE {$tableName} SET {$setPartString}
             {$wherePartString}");
        $res->execute($paramsArray);
    }

    public function insert($tableName, $newRowArray)
    {
        $fieldList = array_keys($newRowArray);
        $fieldListString = implode(', ', $fieldList);
        $newValues = [];
        foreach ($newRowArray as $item => $value)
            $newValues [] = ':' . $item;
        $newValuesString = implode(', ', $newValues);
        $res = $this->pdo->prepare("INSERT INTO {$tableName} ($fieldListString)
            VALUES ($newValuesString)");
        $res->execute($newRowArray);
    }

    public function delete($tableName, $conditionArray){
        $whereParts = [];
        foreach ($conditionArray as $key => $value)
            $whereParts [] = "{$key} = :{$key}";
        $wherePartString = 'WHERE '.implode(' AND ', $whereParts);
        $res = $this->pdo->prepare("DELETE FROM {$tableName} {$wherePartString}");
        $res->execute($conditionArray);
    }
}