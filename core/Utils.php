<?php

namespace core;

class Utils
{
    public static function filterArray($array, $fieldsList)
    {
        $newArray = [];
        foreach ($array as $key => $value)
            if (in_array($key, $fieldsList))
                $newArray[$key] = $value;
        return $newArray;
    }
    public static function normalizationFilesArray($files)
    {
        $NormalnyArreyfiles = array();
        foreach($files['files'] as $k => $l) {
            foreach($l as $i => $v) {
                $NormalnyArreyfiles[$i][$k] = $v;
            }
        }
        return $NormalnyArreyfiles;
    }
}